# Mis arreglos para Bootstrap

## Nuevo componente: nice-input-file.

Hay dos formas de usar este componente:

### HTML auto-generado (la recomendada).

Ejemplo:

``` [HTML]
<input
	data-nice-input-file="
		| placeholder = Elige la fotografía (JPEG)
		| class = like-no-readonly cursor-hand not-blur-on-run-nice-input-file
		| file_template = «[#name#]» ([#getSizeByteFormat#])
		| join_template = ;&#32;
		| button_text = Seleccionar
		| button_class = btn-form-control secondary not-blur-on-run-nice-input-file
	"
	accept=".jpg,.jpeg,.jpe,.jif,.jfif,.jfi,image/jpeg"
	autocomplete="off"
	id="archivo"
	type="file"
/>
```

* El atributo `data-nice-input-file` es removido por lo que no se debe usar como selector para nada.
* Los espacios y saltos de línea obviamente solo son para mejorar la lectura del ejemplo.
* El valor del atributo `data-nice-input-file` es un objeto *ListOne*, si necesitas usar `|` en el valor de alguna de las propiedades simplemente remplaza todos los `|` del ejemplo por algún otro carácter que no vayas a usar, por ejemplo: `&`, `¬`, `-`, etc.
* Si necesitas usar un `=` en los valores de las propiedades no hace falta que cambies nada.
* La propiedad `placeholder` es el atributo `placeholder` del campo de texto. El valor por defecto es `Choose file`.
* La propiedad `class` es el atributo `class` del campo de texto, siempre se incluye `form-control`. El valor por defecto es `like-no-readonly cursor-hand`.
* La propiedad `file_template` define como se representará cada archivo seleccionado. Es un string que se evalúa como una plantilla contra el archivo seleccionado por lo que acepta cualquier propiedad o método (sin parámetros requeridos) de la clase `File` como clave a remplazar, esto incluye las definidas en [fix-js](https://gitlab.com/jmdz/fix-js). A la fecha las disponibles son: `name`, `size` (bytes), `lastModified` (ms desde Epoch), `getSizeByteFormat` y `getLastModifiedAsISODateTime` (como CCYY-MM-DD HH:mm:ss). El valor por defecto es `«[#name#]» ([#getSizeByteFormat#])`.
* La propiedad `join_template` define como se concatenan las representaciones de múltiples archivos. El valor por defecto es `;&#32;` (un `semicolon` seguido de un `space`).
* La propiedad `button_text` es el atributo `value` del botón. El valor por defecto es `Browse`.
* La propiedad `button_class` es el atributo `class` del botón, siempre se incluye `btn`. El valor por defecto es `btn-form-control secondary`.
* Tanto el botón como el campo de texto pierden el foco cuando se selecciona un archivo, para evitar esto incluye en las propiedades `class` y `button_class` (o solo en una) la clase `not-blur-on-run-nice-input-file`.

### Escribir manualmente el HTML.

Ejemplo:

``` [HTML]
<div class="nice-input-file input-group">
	<input
		data-template="; "
		class="form-control like-no-readonly cursor-hand not-blur-on-run-nice-input-file"
		placeholder="Elige la fotografía (JPEG)"
		readonly="readonly"
		autocomplete="off"
		type="text"
	/>
	<input
		data-template="«[#name#]» ([#getSizeByteFormat#])"
		accept=".jpg,.jpeg,.jpe,.jif,.jfif,.jfi,image/jpeg"
		autocomplete="off"
		id="archivo"
		type="file"
	/>
	<div class="input-group-append">
		<input
			class="btn btn-form-control secondary not-blur-on-run-nice-input-file"
			type="button"
			value="Seleccionar"
		/>
	</div>
</div>
```

* El campo de archivo **debe** estar en el medio.
* El campo de archivo **debe** tener un atributo `data-template` valido (véase la propiedad `text_template` en el modo automático).
* El `<div/>` que agrupa los tres campos **debe** tener la clase `nice-input-file`
* El campo de texto **debe** tener un atributo `data-template` valido (véase la propiedad `join_template` en el modo automático).
* Los espacios y saltos de línea obviamente solo son para mejorar la lectura del ejemplo.
* La clase `not-blur-on-run-nice-input-file` es opcional y evita la perdida del foco cuando se selecciona un archivo.

### ¿Por qué no usar el `custom-file` que trae Bootstrap?

Tiene estos problemas:

* Cuando esta vacío no se aclara como los otros placeholders lo que rompe el esquema visual.
* No se puede cambiar el texto o los estilos del botón o de las etiquetas (`<label>`) .
* Necesita JavaScript extra que no se incluye.

## Nuevo componente: nice-link-up.

* Ejemplo:

	``` [HTML]
	<a data-nice-link-up="
		| autohide = on
		| offset = 360
		| speed = 495
		| vertical = -1
		| horizontal = -1
		| zindex = 1035
	">Arriba</a>
	```

* El atributo `data-nice-link-up` es removido por lo que no se debe usar como selector para nada.
* Los espacios y saltos de línea obviamente solo son para mejorar la lectura del ejemplo.
* El valor del atributo `data-nice-link-up` es un objeto *ListOne*, si necesitas usar `|` en el valor de alguna de las propiedades simplemente remplaza todos los `|` del ejemplo por algún otro carácter que no vayas a usar, por ejemplo: `&`, `¬`, `-`, etc. Igual es imposible que necesites usar un `|`.
* Si necesitas usar un `=` en los valores de las propiedades no hace falta que cambies nada. Tampoco lo vas a necesitar.
* La propiedad `autohide` tiene dos valores `on` y `off`. El valor por defecto es `on`.
* La propiedad `offset` son los pixels desde el borde superior de la ventana que debe desplazarse el usuario para activar el botón. El valor por defecto es `360`.
* La propiedad `speed` son los milisegundos de las animaciones. El valor por defecto es `495`.
* La propiedad `vertical` son los pixels desde el borde superior de la ventana donde se muestra el botón. Los valores negativos se miden desde el borde inferior de la ventana. El valor por defecto es `-1`.
* La propiedad `horizontal` son los pixels desde el borde izquierdo de la ventana donde se muestra el botón. Los valores negativos se miden desde el borde derecho de la ventana. El valor por defecto es `-1`.
* Si las propiedades `vertical` y `horizontal` son `0` no se establece la posición del botón y tu la puedes manejar desde CSS.
* La propiedad `zindex` corresponde a la propiedad `z-index` de CSS, solo se aplica si es un entero mayor a cero. El valor por defecto es `1035`.
* La propiedad CSS `cursor` ya no es definida, se puede usar la utilidad *cursor-hand*.

## Nueva utilidad: cursor-hand.

Asigna el cursor *mano* al elemento mientras no tenga un atributo `disabled`.

``` [HTML]
	<button class="cursor-hand">No soy un enlace pero tengo la <em>manito</em></button>
```

## Nueva utilidad: arrows.

Coloca una flecha decorativa antes o después de un elemento. Tal como la de los botones de dropdowns.

Se usa con las clases `arrow-{dirección}-{posición}`. Las posiciones son `before`y `after`. Y las direcciones son `top`, `right`, `bottom` y `left`.

Estas flechas incluyen el espaciado necesario según su posición, para desactivarlo se agrega la clase `arrow-no-margin`. Esto sirve si por alguna razón se oculta el texto del elemento.

## Extensión de utilidad: sizing.

Nuevas clases para limitar el tamaño de un elemento.

``` [HTML]
	<div class="max-vw-100">Max-width 100vw</div>
	<div class="max-vh-100">Max-height 100vh</div>
```


.w-95{width:95%!important;}
.w-90{width:90%!important;}
.w-85{width:85%!important;}
.w-80{width:80%!important;}
.w-70{width:70%!important;}
.w-65{width:65%!important;}
.w-60{width:60%!important;}
.w-55{width:55%!important;}
.w-45{width:45%!important;}
.w-40{width:40%!important;}
.w-35{width:35%!important;}
.w-30{width:30%!important;}
.w-20{width:20%!important;}
.w-15{width:15%!important;}
.w-10{width:10%!important;}
.w-5{width:5%!important;}

.w-66{width:calc(100% / 3 * 2)!important;}
.w-33{width:calc(100% / 3)!important;}



## Extensión de utilidad: text.

Nuevas clases para controlar el tamaño del texto de un elemento.

``` [HTML]
	<div class="text-1">Font-size:0.50rem;</div>
	<div class="text-2">Font-size:0.75rem;</div>
	<div class="text-3">Font-size:1.00rem;</div>
	<div class="text-4">Font-size:1.33rem;</div>
	<div class="text-5">Font-size:2.00rem;</div>
```

## Extensión de contenido: tables.

Una clase `caption-top` para colocar el `<caption/>` de una tabla en la parte de superior (por defecto es colocado en la parte inferior).

``` [HTML]
	<table class="table caption-top">
		<caption>Lista de usuarios</caption>
		<thead>
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Usuario</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>1</th>
				<td>Mauro</td>
				<td>Daino</td>
				<td>@jmdz</td>
			</tr>
		</tbody>
	</table>
```

## Nueva utilidad: highlight-external-links.

Resalta los enlaces externos colocando una flecha doble hacia noreste (`⇗`, `0x21D7`) a continuación de los enlaces.

La clase no se aplica al enlace mismo si no a un contenedor sin limite de profundidad.

``` [HTML]
	<div class="highlight-external-links">
		<a href="http://jmdz.com.ar">enlace externo</a>
	</div>
```

La flecha mostrada esta almacenada en la variable `--highlight-external-links-text`.

### TODO

* Analizar casos `dir="rtl"`.

## Extensión de componente: forms.

Para los casos donde una campo `readonly` es necesario que se muestre como si no lo fuera se puede usar la clase `like-no-readonly`.

## Extensión de componente: buttons.

A veces hace falta que un botón que parezca un control de formulario, para eso es la clase `btn-form-control`.

Como los colores usados no están en variables se crean dos `--form-control-color` y `--form-control-border-color`.

## Corrección de componentes: buttons, forms, table.

Todas estas correcciones se activan a los elementos descendientes de un contenedor (por ejemplo `<body/>`) que tenga la clase `fix-bs`.

* Cursor `not-allowed` a los elementos con clase `btn` o `form-control` que estén deshabilitados.

* Color de fondo más oscuro para la clase `table-active` y uno aún más oscuro para cuando coincide con la clase `table-hover`.


## Nuevo componente: bs_modal.

Es una función JavaScript para cuando es necesario crear modales de forma completamente dinámica. Recibe un único parámetro que debe ser un objeto plano que puede tener las siguientes propiedades:

* `(bool) unique = true`, indica si se deben cerrar todos los modales abiertos al abrir este.
* `(string) size = ''`, es el tamaño del modal definido en la [documentación de Bootstrap](https://getbootstrap.com/docs/4.3/components/modal/#optional-sizes), si se asigna un valor invalido se ignora, los valores validos son:
	* `'modal-sm'` (`max-width:300px`)
	* `''` (`max-width:500px`)
	* `'modal-lg'` (`max-width:800px`)
	* `'modal-xl'` (`max-width:1140px`)
	* `'sm'` (alias de `'modal-sm'`)
	* `'lg'` (alias de `'modal-lg'`)
	* `'xl'` (alias de `'modal-xl'`)
* `(string) title = ''`, es el título del modal, si esta vacío no se muestra el elemento de título. Véase la propiedad `close` a continuación.
* `(string) close = 'Close'`, es el tooltip (atributo `title`) del botón cerrar, si esta vacío no se incluye el botón cerrar. Si tanto la propiedad `close` como la propiedad `title` son la cadenas nulas  (`''`) no se muestra el elemento de encabezado.
* `(string) body = ''`, es el código del cuerpo del modal, si no empieza por `<` se lo envuelve en un `<p class="text-center m-0">`.
* `(string) footer = ''`, es el código del pie del modal, si no empieza por `<` se lo envuelve en un `<p class="text-right m-0">`. Si es la cadena vacía (`''`) no se muestra el elemento del pie.
* `(int>=0) timeout = 0`, es la cantidad de milisegundos a esperar antes de cerrar automáticamente el modal, si es `0` el modal no se cierra automáticamente.
* `(function|false) onload = false`, es una función a ejecutarse inmediatamente después de abrir el modal o el booleano `false` para no ejecutar nada. Esta función se ejecuta antes de asignar el foco (véase siguiente la propiedad).
* `(bool) focus = true`, indica si se debe asignar el foco al primer elemento que pueda recibirlo en el cuerpo, pie o encabezado (en ese orden).
* `(bool) clickout = true`, indica si se debe cerrar el modal al hacer clic fuera del modal.
* `(bool) escape = true`, indica si se debe cerrar el modal al presionar la tecla `escape`.

La función retorna el objeto *jQuery* correspondiente al `<div class="modal">`. Los modales generados con esta función se destruyen cuando se cierran.

Todos los elementos generados tienen `id` basados en el principal que puede obtenerse rápidamente del objeto devuelto.

Por ejemplo:

``` [JS]
	bs_modal({
		unique:true,
		size:'xl',
		title:'Título',
		close:'Close',
		body:'Texto',
		footer:'<div class="float-right"><button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cerrar</button></div>',
		timeout:0,
		onload:false,
		focus:true,
		clickout:true,
		escape:true,
	});
```

Devolvería algo un objeto *jQuery* similar a `$('#MODAL_1560219109949_954090816')` y generaría este código:

``` [HTML]
	<div id="MODAL_1560219109949_954090816" data-keyboard="true" data-backdrop="true" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MODAL_1560219109949_954090816_TITLE" aria-modal="true">
		<div id="MODAL_1560219109949_954090816_DIALOG" class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
			<div id="MODAL_1560219109949_954090816_CONTENT" class="modal-content">
				<div id="MODAL_1560219109949_954090816_HEADER" class="modal-header">
					<h5 id="MODAL_1560219109949_954090816_TITLE" class="modal-title">Título</h5>
					<button id="MODAL_1560219109949_954090816_CLOSE" type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span id="MODAL_1560219109949_954090816_CLOSE_ICON" title="Close" aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="MODAL_1560219109949_954090816_BODY" class="modal-body">
					<p id="MODAL_1560219109949_954090816_BODY_TEXT" class="text-center m-0">Texto</p>
				</div>
				<div id="MODAL_1560219109949_954090816_FOOTER" class="modal-footer">
					<div class="float-right">
						<button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
```


## Ejemplos en vivo.

Todos mis experimentos (que para eso están): [jmdz.com.ar/apps](http://jmdz.com.ar/apps) y [gitlab/jmdz/my-web](https://gitlab.com/jmdz/my-web).
