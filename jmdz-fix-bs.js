
function bs_modal($config){
	let sizes={
		'modal-sm':'modal-sm',
		'':'',
		'modal-lg':'modal-lg',
		'modal-xl':'modal-xl',
		default:'',
		'sm':'modal-sm',
		'lg':'modal-lg',
		'xl':'modal-xl',
	};
	let default_config={
		unique:true,
		size:'',
		title:'',
		close:'Close',
		body:'',
		footer:'',
		timeout:0,
		onload:false,
		focus:true,
		clickout:true,
		escape:true,
	};
	let config=$.extend({},default_config,$config);
	config.unique=config.unique?true:false;
	if(!isDefined(sizes[config.size]))config.size=sizes.default;
	if(typeof(config.title)!=='string')config.title=config.title?config.title.toString():'';
	if(typeof(config.close)!=='string')config.close=config.close?config.close.toString():'';
	if(typeof(config.body)!=='string')config.body=config.body?config.body.toString():'';
	if(typeof(config.footer)!=='string')config.footer=config.footer?config.footer.toString():'';
	config.timeout=Math.abs(parseInt(config.timeout,10));if(isNaN(config.timeout))config.timeout=0;
	if(typeof(config.onload)!=='function')config.onload=false;
	config.focus=config.focus?true:false;
	config.clickout=config.clickout?true:false;
	config.escape=config.escape?true:false;
	if(config.unique){
		$('.modal').modal('hide');
	}
	let id=Date.now()+'_'+Math.floor(Math.random()*1000000000);
	{$('body').append(
		'<div id="MODAL_'+id+'" data-keyboard="'+(config.escape?'true':'false')+'" data-backdrop="'+(config.clickout?'true':'static')+'" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"'+(config.title?' aria-labelledby="MODAL_'+id+'_TITLE"':'')+'>'
			+'<div id="MODAL_'+id+'_DIALOG" class="modal-dialog '+sizes[config.size]+' modal-dialog-centered modal-dialog-scrollable" role="document">'
				+'<div id="MODAL_'+id+'_CONTENT" class="modal-content">'
					+(
						config.title || config.close
						?
							'<div id="MODAL_'+id+'_HEADER" class="modal-header">'
								+(config.title?'<h5 id="MODAL_'+id+'_TITLE" class="modal-title">'+config.title+'</h5>':'')
								+(config.close?'<button id="MODAL_'+id+'_CLOSE" type="button" class="close" data-dismiss="modal" aria-label="'+config.close+'"><span id="MODAL_'+id+'_CLOSE_ICON" title="'+config.close+'" aria-hidden="true">&times;</span></button>':'')
							+'</div>'
						:''
					)
					+'<div id="MODAL_'+id+'_BODY" class="modal-body">'
						+(config.body.slice(0,1)!=='<'?'<p id="MODAL_'+id+'_BODY_TEXT" class="text-center m-0">':'')
						+config.body
						+(config.body.slice(0,1)!=='<'?'</p>':'')
					+'</div>'
					+(
						config.footer
						?
							'<div id="MODAL_'+id+'_FOOTER" class="modal-footer">'
								+(config.footer.slice(0,1)!=='<'?'<p id="MODAL_'+id+'_FOOTER_TEXT" class="text-right m-0">':'')
								+config.footer
								+(config.footer.slice(0,1)!=='<'?'</p>':'')
							+'</div>'
						:''
					)
				+'</div>'
			+'</div>'
		+'</div>'
	);}
	let modal=$('#MODAL_'+id);
	if(config.onload){
		modal.on('shown.bs.modal',config.onload);
	}
	if(config.focus){
		modal.on('shown.bs.modal',function(){
			var s='a[href]:not([tabindex="-1"])'
				+',area[href]:not([tabindex="-1"])'
				+',input:not([disabled]):not([tabindex="-1"]):not([type="hidden"])'
				+',select:not([disabled]):not([tabindex="-1"])'
				+',textarea:not([disabled]):not([tabindex="-1"])'
				+',button:not([disabled]):not([tabindex="-1"])'
				+',iframe:not([tabindex="-1"])'
				+',[tabindex]:not([tabindex="-1"])'
				+',[contentEditable=true]:not([tabindex="-1"])'
			;
			if(modal.find('div.modal-body').find(s).length){
				modal.find('div.modal-body').find(s).first().focus();
			}
			else if(modal.find('div.modal-footer').find(s).length){
				modal.find('div.modal-footer').find(s).first().focus();
			}
			else if(modal.find('div.modal-header').find(s).length){
				modal.find('div.modal-header').find(s).first().focus();
			}
		});
	}
	modal.on('hidden.bs.modal',function(){modal.remove();});
	if(config.timeout){
		setTimeout(function(){
			$('#MODAL_'+id).modal('hide');
		},config.timeout);
	}
	modal.modal();
	return modal;
/*function bs_modal*/}

(function($){$(document).ready(function(){

	/*extend:components:nice-input-file*/{
	$('input[type="file"][data-nice-input-file]').each(function($i,$e){
		let e=$($e);
		let o=Object.assign(
			{
				placeholder:'Choose file'
				,class:'like-no-readonly cursor-hand'
				,file_template:'«[#name#]» ([#getSizeByteFormat#])'
				,join_template:';&#32;'
				,button_text:'Browse'
				,button_class:'btn-form-control secondary'
			}
			,Object.fromListOne(e.attr('data-nice-input-file').trim(),'=')
		);
		e.removeAttr('data-nice-input-file');
		e.attr('data-template',o.file_template);
		let d=$(''
			+'<div class="nice-input-file input-group">'
			+'<input class="form-control '+o.class+'" data-template="'+o.join_template+'" placeholder="'+o.placeholder+'" readonly="readonly" type="text" autocomplete="off"/>'
				+'<div class="input-group-append">'
					+'<input class="btn '+o.button_class+'" type="button" value="'+o.button_text+'"/>'
				+'</div>'
			+'</div>'
		);
		e.before(d);
		e=e.detach();
		d.find('input').first().after(e);
		e.data(o);
	});
	$('.nice-input-file [type="text"],.nice-input-file [type="button"]').on('click.nice-input-file',function($event){
		$($event.target).closest('.nice-input-file').find('[type="file"]').trigger('click');
	});
	$('.nice-input-file [type="file"]').on('change.nice-input-file',function($event){
		var f=$($event.target);
		var d=f.closest('.nice-input-file');
		setTimeout(
			function(){
				d.find(
					'[type="text"]:not(.not-blur-on-run-nice-input-file):focus'
					+',[type="button"]:not(.not-blur-on-run-nice-input-file):focus'
				).blur();
			}
			,45
		);
		var o=[].slice.call($event.target.files).map(function($v){
			log($v);
			return f.attr('data-template').template($v);
		});
		var t=d.find('[type="text"]');
		t.val(o.join(t.attr('data-template')));
	});
	$('.nice-input-file [type="file"]').each(function($i,$e){
		var e=$($e);
		if(e.is('[autocomplete]')){
			e.closest('.nice-input-file').find('[type="text"]').attr(
				'autocomplete'
				,e.attr('autocomplete')
			);
		}
		if(e.is('[disabled]')){
			e.closest('.nice-input-file').find('[type="text"],[type="button"]').attr(
				'disabled'
				,e.attr('disabled')
			);
		}
		e.trigger('change.nice-input-file');
		$e.observeAttribute(
			'disabled'
			,function($event){
				var f=$($event.target);
				f.closest('.nice-input-file').find('[type="text"],[type="button"]')
					.prop('disabled',f.prop('disabled'))
				;
			}
		);
	});
	/*extend:components:nice-input-file*/}

	/*extend:components:nice-link-up*/{
	$('[data-nice-link-up]').each(function($i,$e){
		let e=$($e);
		let o=Object.assign(
			{
				autohide:'on',
				offset:'360',
				speed:'495',
				vertical:'-1',
				horizontal:'-1',
				zindex:'1035',
			}
			,Object.fromListOne(e.attr('data-nice-link-up').trim(),'=')
		);
		e.removeAttr('data-nice-link-up');
		if(parseInt(o.zindex,10)>0){
			e.css('z-index',o.zindex);
		}
		if(o.autohide==='on'){
			e.css('display','none');
		}
		o.vertical=+o.vertical;
		o.horizontal=+o.horizontal;
		if(o.vertical&&o.horizontal){
			e.css('position','fixed');
			if(o.vertical<0){
				e.css('bottom',-o.vertical);
			}
			else{
				e.css('top',o.vertical);
			}
			if(o.horizontal<0){
				e.css('right',-o.horizontal);
			}
			else{
				e.css('left',o.horizontal);
			}
		}
		e.on('click',function(){
			$('html,body').animate({scrollTop:0},o.speed);
		});
		$(window).on('scroll',function(){
			if(o.autohide==='on'){
				if(o.offset<$(window).scrollTop()){
					e.fadeIn(o.speed);
				}
				else{
					e.fadeOut(o.speed);
				}
			}
		});
	});
	/*extend:components:nice-link-up*/}

});}(jQuery));
